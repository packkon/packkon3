﻿// InterfaceMaster.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
// using System.Collections;
// using Mijinko;

// #if UNITY_EDITOR
// using UnityEditor;
// #endif

public class InterfaceMaster : MonoBehaviour
{

	static public InterfaceMaster master;

	public bool[] sensing;

	[SerializeField]
	KeyCode[] keyCode = new KeyCode[]{
		KeyCode.A,
		KeyCode.S,
		KeyCode.D,
		KeyCode.F,
		KeyCode.G,
		KeyCode.H,
		KeyCode.J,
		KeyCode.K,
		KeyCode.L,
		KeyCode.Semicolon,
		KeyCode.Colon,
		KeyCode.RightBracket
	};


	void Awake ()
	{
		master = this;
	}

	void Start ()
	{
		sensing = new bool[GameMaster.master.numOfNote];
	}
	
	void Update ()
	{
		Debug_KeyboardInput();
	}


	void Debug_KeyboardInput() {
		for (int i=0; i<Mathf.Min(keyCode.Length, GameMaster.master.numOfNote); i++) {
			sensing[i] = Input.GetKey(keyCode[i]);
		}
	}
}



// #if UNITY_EDITOR
// [CustomEditor(typeof(InterfaceMaster))]
// public class InterfaceMasterEdiroe : Editor{

// 	static bool debugMode = false;
// 	InterfaceMaster targetCompo;
	
// 	public override void OnInspectorGUI()
// 	{
// 		targetCompo = (InterfaceMaster) target;

// 		debugMode = EditorGUILayout.Toggle( "デバッグモード", debugMode );
// 		EditorGUILayout.Space();

// 		// debugMode == true だったら、もとのインスペクターを表示する。
// 		if (debugMode) {
// 	        DrawDefaultInspector();
// 	        serializedObject.Update ();
// 		} else {
// 			Custom();
// 		}
// 	}

// 	void Custom()
// 	{ // ここにEditorGUILayoutしていく
		
// 	}
// }
// #endif