﻿// GameMaster.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
// using System.Collections;
// using Mijinko;

// #if UNITY_EDITOR
// using UnityEditor;
// #endif

public class GameMaster : MonoBehaviour
{
	static public GameMaster master;

	public int numOfNote = 6;

	void Awake ()
	{
		master = this;
	}

	// void Start ()
	// {
	// }
	
	// void Update ()
	// {
	// }
}



// #if UNITY_EDITOR
// [CustomEditor(typeof(GameMaster))]
// public class GameMasterEdiroe : Editor{

// 	static bool debugMode = false;
// 	GameMaster targetCompo;
	
// 	public override void OnInspectorGUI()
// 	{
// 		targetCompo = (GameMaster) target;

// 		debugMode = EditorGUILayout.Toggle( "デバッグモード", debugMode );
// 		EditorGUILayout.Space();

// 		// debugMode == true だったら、もとのインスペクターを表示する。
// 		if (debugMode) {
// 	        DrawDefaultInspector();
// 	        serializedObject.Update ();
// 		} else {
// 			Custom();
// 		}
// 	}

// 	void Custom()
// 	{ // ここにEditorGUILayoutしていく
		
// 	}
// }
// #endif