﻿// NoteSystem.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
// using System.Collections;
// using Mijinko;

// #if UNITY_EDITOR
// using UnityEditor;
// #endif

public class NoteSystem : MonoBehaviour
{
	// void Awake ()
	// {
	// }

	// void Start ()
	// {
	// }
	
	void Update ()
	{
		if (transform.localPosition.y < NoteMaster.master.judgeHeight) {
			Destroy(gameObject);
		}
	}
}



// #if UNITY_EDITOR
// [CustomEditor(typeof(NoteSystem))]
// public class NoteSystemEdiroe : Editor{

// 	static bool debugMode = false;
// 	NoteSystem targetCompo;
	
// 	public override void OnInspectorGUI()
// 	{
// 		targetCompo = (NoteSystem) target;

// 		debugMode = EditorGUILayout.Toggle( "デバッグモード", debugMode );
// 		EditorGUILayout.Space();

// 		// debugMode == true だったら、もとのインスペクターを表示する。
// 		if (debugMode) {
// 	        DrawDefaultInspector();
// 	        serializedObject.Update ();
// 		} else {
// 			Custom();
// 		}
// 	}

// 	void Custom()
// 	{ // ここにEditorGUILayoutしていく
		
// 	}
// }
// #endif