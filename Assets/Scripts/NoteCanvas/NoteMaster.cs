﻿// NoteMaster.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
// using System.Collections;
// using Mijinko;

// #if UNITY_EDITOR
// using UnityEditor;
// #endif

public class NoteMaster : MonoBehaviour
{

	static public NoteMaster master;

	public int judgeHeight;

	[SerializeField]
	int noteInterval_x = 100;
	[SerializeField]
	float noteFallingSpeed = 5f;
	[SerializeField]
	GameObject noteObject_pref;
	// [SerializeField, Space(10)]
	// これをtrueにしたときにGizmosの座標をカメラからみたワールド座標で計算するようにする
	// bool drawGizmosOnGameView;

	int numOfNote = 6;
	RectTransform canvas_rt;

	void Awake ()
	{
		master = this;
		if (!canvas_rt) {
			canvas_rt = GetComponent<RectTransform>();
		}
	}

	void Start ()
	{
		numOfNote = GameMaster.master.numOfNote;
	}
	
	void Update ()
	{
		Debug_NoteAppear();
	}


	void Debug_NoteAppear() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			AppearNoteObject(Random.Range(0, 6));
		}
	}

	public void AppearNoteObject(int pos)
	{
		GameObject note_go = Instantiate(noteObject_pref);
		note_go.transform.SetParent(transform, false);
		note_go.transform.localPosition = new Vector3(
			noteInterval_x * (pos - ((float)numOfNote - 1) / 2),
			canvas_rt.sizeDelta.y / 2,
			0);
		note_go.GetComponent<Rigidbody2D>().velocity = Vector2.up * -noteFallingSpeed * canvas_rt.lossyScale.y;
	}


	// -----

	// void OnDrawGizmos()
	void OnDrawGizmosSelected()
	{
		if (canvas_rt == null) {
			canvas_rt = GetComponent<RectTransform>();
		}
		DrawJudgeHeightGizmo(true);
		DrawPathOfNoteGizmo(true);
	}
	void OnDrawGizmos()
	{
		if (canvas_rt == null) {
			canvas_rt = GetComponent<RectTransform>();
		}
		DrawJudgeHeightGizmo(false);
		DrawPathOfNoteGizmo(false);
	}

	void DrawJudgeHeightGizmo(bool drawOnCanvas)
	{
		float lineHeight = canvas_rt.position.y + judgeHeight * canvas_rt.lossyScale.y;
		Gizmos.color = Color.green;
		if (drawOnCanvas) {
			Gizmos.DrawLine(new Vector3(0, lineHeight, 0), new Vector3(canvas_rt.position.x * 2, lineHeight, 0));
		} else {
			Gizmos.DrawLine(Camera.main.ScreenToWorldPoint(new Vector3(0, lineHeight, Camera.main.near)), Camera.main.ScreenToWorldPoint(new Vector3(canvas_rt.position.x * 2, lineHeight, Camera.main.near)));
		}
	}

	void DrawPathOfNoteGizmo(bool drawOnCanvas)
	{
		Gizmos.color = Color.yellow;
		for (int i=0; i<numOfNote; i++) {
			float lineX = canvas_rt.position.x + noteInterval_x * (i - ((float)numOfNote - 1) / 2) * canvas_rt.lossyScale.x;
			if (drawOnCanvas) {
				Gizmos.DrawLine(new Vector3(lineX, 0, 0), new Vector3(lineX, canvas_rt.position.y * 2, 0));
			} else {
				Gizmos.DrawLine(Camera.main.ScreenToWorldPoint(new Vector3(lineX, 0, Camera.main.near)), Camera.main.ScreenToWorldPoint(new Vector3(lineX, canvas_rt.position.y * 2, Camera.main.near)));
			}
		}
	}

}



// #if UNITY_EDITOR
// [CustomEditor(typeof(NoteMaster))]
// public class NoteMasterEdiroe : Editor{

// 	static bool debugMode = false;
// 	NoteMaster targetCompo;
	
// 	public override void OnInspectorGUI()
// 	{
// 		targetCompo = (NoteMaster) target;

// 		debugMode = EditorGUILayout.Toggle( "デバッグモード", debugMode );
// 		EditorGUILayout.Space();

// 		// debugMode == true だったら、もとのインスペクターを表示する。
// 		if (debugMode) {
// 	        DrawDefaultInspector();
// 	        serializedObject.Update ();
// 		} else {
// 			Custom();
// 		}
// 	}

// 	void Custom()
// 	{ // ここにEditorGUILayoutしていく
		
// 	}
// }
// #endif